package mg.kotleta.bloghistory.service.applicative.ws.post

import mg.kotleta.bloghistory.data.dto.PostDto
import mg.kotleta.bloghistory.service.applicative.ws.AbstractHttpWebService
import mg.kotleta.bloghistory.service.bdl.post.PostBdl


object PostHttp : AbstractHttpWebService(), IPostSA {

  override fun getPostById(id: Long, before: (() -> Unit)?, success: (PostDto) -> Unit, failure: (Throwable?) -> Unit, done: (() -> Unit)?) {
    performRequest(PostBdl.getPostById(id), before, success, failure, done)
  }

  override fun getPosts(before: (() -> Unit)?, success: (List<PostDto>) -> Unit, failure: (Throwable?) -> Unit, done: (() -> Unit)?) {
    performRequest(PostBdl.getPosts(), before, success, failure, done)
  }
}