package mg.kotleta.bloghistory.service.applicative.ws.comment

import mg.kotleta.bloghistory.data.constants.BeforeClosure
import mg.kotleta.bloghistory.data.constants.DoneClosure
import mg.kotleta.bloghistory.data.constants.FailureClosure
import mg.kotleta.bloghistory.data.constants.SuccessClosure
import mg.kotleta.bloghistory.data.dto.CommentDto

interface ICommentSA {

  fun getComments(options: Map<String, String> = HashMap(), before: BeforeClosure = null, success: SuccessClosure<List<CommentDto>>, failure: FailureClosure, done: DoneClosure = null)

  fun getCommentsByPostId(postId: String, before: BeforeClosure = null, success: SuccessClosure<List<CommentDto>>, failure: FailureClosure, done: DoneClosure = null)

}