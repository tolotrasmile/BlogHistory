package mg.kotleta.bloghistory.service.bdl.comment

import io.reactivex.Observable
import mg.kotleta.bloghistory.common.RetrofitFactory
import mg.kotleta.bloghistory.data.dto.CommentDto
import mg.kotleta.bloghistory.service.bdl.AbstractBdl


object CommentBdl : AbstractBdl(), ICommentBdl {

  override fun getCommentsByPostId(postId: String): Observable<List<CommentDto>> =
      RetrofitFactory.createService(ICommentBdl::class.java, baseUrl).getCommentsByPostId(postId)

  override fun getComments(options: Map<String, String>): Observable<List<CommentDto>> =
      RetrofitFactory.createService(ICommentBdl::class.java, baseUrl).getComments(options)
}
