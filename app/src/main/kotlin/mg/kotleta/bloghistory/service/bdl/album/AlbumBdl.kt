package mg.kotleta.bloghistory.service.bdl.album

import io.reactivex.Observable
import mg.kotleta.bloghistory.common.RetrofitFactory
import mg.kotleta.bloghistory.data.dto.AlbumDto
import mg.kotleta.bloghistory.service.bdl.AbstractBdl

/**
 * Created by admin on 30/08/2017.
 */
object AlbumBdl : AbstractBdl(), IAlbumBdl {
  override fun getAlbums(options: Map<String, String>): Observable<List<AlbumDto>> {
    return RetrofitFactory.createService(IAlbumBdl::class.java, baseUrl).getAlbums(options)
  }
}