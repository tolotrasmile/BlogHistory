package mg.kotleta.bloghistory.service.applicative.ws.user

import mg.kotleta.bloghistory.data.dto.UserDto


interface IUserSA {

  data class AuthParametersDto(val email: String = "", val password: String)

  fun getUsers(before: (() -> Unit)? = null, success: (List<UserDto>) -> Unit, failure: (Throwable?) -> Unit, done: (() -> Unit)? = null)

  fun addUser(userDto: UserDto, before: (() -> Unit)? = null, success: (UserDto) -> Unit, failure: (Throwable?) -> Unit, done: (() -> Unit)? = null)

  fun authenticate(params: AuthParametersDto, before: (() -> Unit)? = null, success: (UserDto) -> Unit, failure: (Throwable?) -> Unit, done: (() -> Unit)? = null)

}