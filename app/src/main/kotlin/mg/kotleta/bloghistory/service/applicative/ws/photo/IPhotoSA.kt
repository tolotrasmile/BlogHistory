package mg.kotleta.bloghistory.service.applicative.ws.photo

import mg.kotleta.bloghistory.data.constants.BeforeClosure
import mg.kotleta.bloghistory.data.constants.DoneClosure
import mg.kotleta.bloghistory.data.constants.FailureClosure
import mg.kotleta.bloghistory.data.constants.SuccessClosure
import mg.kotleta.bloghistory.data.dto.PhotoDto

/**
 * Created by admin on 31/08/2017.
 */
interface IPhotoSA {

  fun getPhotos(options: Map<String, String> = HashMap(), before: BeforeClosure = null, success: SuccessClosure<List<PhotoDto>>, failure: FailureClosure, done: DoneClosure = null)
}
