package mg.kotleta.bloghistory.service.preferences

import android.content.Context
import android.content.SharedPreferences
import org.androidannotations.annotations.AfterInject
import org.androidannotations.annotations.EBean
import org.androidannotations.annotations.RootContext

@EBean(scope = EBean.Scope.Singleton)
open class Preferences : IPreferences {
  private var pref: SharedPreferences? = null

  @RootContext
  @JvmField
  var context: Context? = null

  @AfterInject
  protected fun afterInject() {
    pref = context!!.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
  }

  override fun putBool(key: String, value: Boolean) {
    val editor = pref!!.edit()
    editor.putBoolean(key, value)
    editor.apply()
  }

  override fun getBool(key: String): Boolean = pref!!.getBoolean(key, false)

  override fun putString(key: String, value: String) {
    val editor = pref!!.edit()
    editor.putString(key, value)
    editor.apply()
  }

  override fun getString(key: String): String = pref!!.getString(key, null)

  override fun putInt(key: String, value: Int) {
    val editor = pref!!.edit()
    editor.putInt(key, value)
    editor.apply()
  }

  override fun getInt(key: String): Int = pref!!.getInt(key, -1)

  override fun getInt(key: String, defaultValue: Int): Int = pref!!.getInt(key, defaultValue)

  companion object {
    private val PREF_NAME = "kotleta_prefs"
  }

}