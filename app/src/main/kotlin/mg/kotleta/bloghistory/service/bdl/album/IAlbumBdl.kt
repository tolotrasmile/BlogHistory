package mg.kotleta.bloghistory.service.bdl.album

import io.reactivex.Observable
import mg.kotleta.bloghistory.data.dto.AlbumDto
import retrofit2.http.GET
import retrofit2.http.QueryMap

/**
 * Created by admin on 30/08/2017.
 */
interface IAlbumBdl {
  @GET("albums")
  fun getAlbums(@QueryMap options: Map<String, String> = HashMap()): Observable<List<AlbumDto>>
}
