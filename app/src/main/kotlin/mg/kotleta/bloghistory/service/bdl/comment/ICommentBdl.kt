package mg.kotleta.bloghistory.service.bdl.comment

import io.reactivex.Observable
import mg.kotleta.bloghistory.data.dto.CommentDto
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.QueryMap


interface ICommentBdl {

  @GET("comments")
  fun getComments(@QueryMap options: Map<String, String> = HashMap()): Observable<List<CommentDto>>

  @GET("posts/{postId}/comments")
  fun getCommentsByPostId(@Path("postId") postId: String): Observable<List<CommentDto>>

}