package mg.kotleta.bloghistory.service.bdl.user

import io.reactivex.Observable
import mg.kotleta.bloghistory.data.dto.UserDto
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface IUserBdl {

  @GET("users")
  fun getUsers(): Observable<List<UserDto>>

  @GET("users/{id}")
  fun getUserById(@Query("id") id: Int): Observable<UserDto>

  @POST("users")
  fun addUser(@Body userDto: UserDto): Observable<UserDto>
}
