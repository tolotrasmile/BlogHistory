package mg.kotleta.bloghistory.service.applicative.ws

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import mg.kotleta.bloghistory.data.constants.BeforeClosure
import mg.kotleta.bloghistory.data.constants.DoneClosure
import mg.kotleta.bloghistory.data.constants.FailureClosure
import mg.kotleta.bloghistory.data.constants.SuccessClosure

abstract class AbstractHttpWebService {

  protected fun <T> performRequest(request: Observable<T>, before: BeforeClosure, success: SuccessClosure<T>, failure: FailureClosure, done: DoneClosure) {

    before?.let { before() }

    request.subscribeOn(Schedulers.newThread())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(
            { response ->
              handleResponse<T>(response, success)
              done?.let { done() }
            },
            { error ->
              handleError(error, failure)
              done?.let { done() }
            }
        )
  }

  private fun <T> handleResponse(response: T, success: SuccessClosure<T>) {
    success(response)
  }

  private fun handleError(error: Throwable?, failure: FailureClosure) {
    failure(error)
  }

}