package mg.kotleta.bloghistory.service.bdl.post

import io.reactivex.Observable
import mg.kotleta.bloghistory.common.RetrofitFactory
import mg.kotleta.bloghistory.data.dto.PostDto
import mg.kotleta.bloghistory.service.bdl.AbstractBdl


object PostBdl : AbstractBdl(), IPostBdl {

  override fun getPostById(id: Long): Observable<PostDto> =
      RetrofitFactory.createService(IPostBdl::class.java, baseUrl).getPostById(id)

  override fun getPosts(): Observable<List<PostDto>> =
      RetrofitFactory.createService(IPostBdl::class.java, baseUrl).getPosts()
}
