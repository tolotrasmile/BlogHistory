package mg.kotleta.bloghistory.service.applicative.ws.album

import mg.kotleta.bloghistory.data.constants.BeforeClosure
import mg.kotleta.bloghistory.data.constants.DoneClosure
import mg.kotleta.bloghistory.data.constants.FailureClosure
import mg.kotleta.bloghistory.data.constants.SuccessClosure
import mg.kotleta.bloghistory.data.dto.AlbumDto


interface IAlbumSA {

  fun getAlbums(options: Map<String, String> = HashMap(), before: BeforeClosure = null, success: SuccessClosure<List<AlbumDto>>, failure: FailureClosure, done: DoneClosure = null)
}