package mg.kotleta.bloghistory.service.bdl.post

import io.reactivex.Observable
import mg.kotleta.bloghistory.data.dto.PostDto
import retrofit2.http.GET
import retrofit2.http.Query


interface IPostBdl {

  @GET("posts/{id}")
  fun getPostById(@Query("id") id: Long): Observable<PostDto>

  @GET("posts")
  fun getPosts(): Observable<List<PostDto>>
}
