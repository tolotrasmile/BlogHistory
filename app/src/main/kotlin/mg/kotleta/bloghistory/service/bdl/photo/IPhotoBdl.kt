package mg.kotleta.bloghistory.service.bdl.photo

import io.reactivex.Observable
import mg.kotleta.bloghistory.data.dto.PhotoDto
import retrofit2.http.GET
import retrofit2.http.QueryMap

/**
 * Created by admin on 30/08/2017.
 */
interface IPhotoBdl {

  @GET("photos")
  fun getPhotos(@QueryMap options: Map<String, String>): Observable<List<PhotoDto>>
}
