package mg.kotleta.bloghistory.service.bdl.user

import io.reactivex.Observable
import mg.kotleta.bloghistory.common.RetrofitFactory
import mg.kotleta.bloghistory.data.dto.UserDto
import mg.kotleta.bloghistory.service.bdl.AbstractBdl


object UserBdl : AbstractBdl(), IUserBdl {

  override fun getUsers(): Observable<List<UserDto>> = RetrofitFactory.createService(IUserBdl::class.java, baseUrl).getUsers()

  override fun getUserById(id: Int): Observable<UserDto> = RetrofitFactory.createService(IUserBdl::class.java, baseUrl).getUserById(id)

  override fun addUser(userDto: UserDto): Observable<UserDto> = RetrofitFactory.createService(IUserBdl::class.java, baseUrl).addUser(userDto)

}
