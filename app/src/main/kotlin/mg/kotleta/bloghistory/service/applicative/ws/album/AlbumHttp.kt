package mg.kotleta.bloghistory.service.applicative.ws.album

import mg.kotleta.bloghistory.data.constants.BeforeClosure
import mg.kotleta.bloghistory.data.constants.DoneClosure
import mg.kotleta.bloghistory.data.constants.FailureClosure
import mg.kotleta.bloghistory.data.constants.SuccessClosure
import mg.kotleta.bloghistory.data.dto.AlbumDto
import mg.kotleta.bloghistory.service.applicative.ws.AbstractHttpWebService
import mg.kotleta.bloghistory.service.bdl.album.AlbumBdl

/**
 * Created by admin on 31/08/2017.
 */
object AlbumHttp : AbstractHttpWebService(), IAlbumSA {
  override fun getAlbums(options: Map<String, String>, before: BeforeClosure, success: SuccessClosure<List<AlbumDto>>, failure: FailureClosure, done: DoneClosure) {
    performRequest(AlbumBdl.getAlbums(options), before, success, failure, done)
  }
}