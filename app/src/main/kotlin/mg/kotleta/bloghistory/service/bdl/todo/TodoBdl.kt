package mg.kotleta.bloghistory.service.bdl.todo

import io.reactivex.Observable
import mg.kotleta.bloghistory.common.RetrofitFactory
import mg.kotleta.bloghistory.data.dto.TodoDto
import mg.kotleta.bloghistory.service.bdl.AbstractBdl
import retrofit2.http.QueryMap

/**
 * Created by admin on 30/08/2017.
 */
object TodoBdl : AbstractBdl(), ITodoBdl {
  override fun getTodos(@QueryMap options: Map<String, String>): Observable<List<TodoDto>> =
      RetrofitFactory.createService(ITodoBdl::class.java, baseUrl).getTodos(options)
}
