package mg.kotleta.bloghistory.service.bdl.todo

import io.reactivex.Observable
import mg.kotleta.bloghistory.data.dto.TodoDto
import retrofit2.http.GET
import retrofit2.http.QueryMap

/**
 * Created by admin on 30/08/2017.
 */
interface ITodoBdl {
  @GET("todos")
  fun getTodos(@QueryMap options: Map<String, String>): Observable<List<TodoDto>>
}
