package mg.kotleta.bloghistory.service.applicative.ws.comment

import mg.kotleta.bloghistory.data.constants.BeforeClosure
import mg.kotleta.bloghistory.data.constants.DoneClosure
import mg.kotleta.bloghistory.data.constants.FailureClosure
import mg.kotleta.bloghistory.data.constants.SuccessClosure
import mg.kotleta.bloghistory.data.dto.CommentDto
import mg.kotleta.bloghistory.service.applicative.ws.AbstractHttpWebService
import mg.kotleta.bloghistory.service.bdl.comment.CommentBdl

object CommentHttp : AbstractHttpWebService(), ICommentSA {

  override fun getComments(options: Map<String, String>, before: BeforeClosure, success: SuccessClosure<List<CommentDto>>, failure: FailureClosure, done: DoneClosure) {
    performRequest(CommentBdl.getComments(options), before, success, failure, done)
  }

  override fun getCommentsByPostId(postId: String, before: BeforeClosure, success: SuccessClosure<List<CommentDto>>, failure: FailureClosure, done: DoneClosure) {
    performRequest(CommentBdl.getCommentsByPostId(postId), before, success, failure, done)
  }
}