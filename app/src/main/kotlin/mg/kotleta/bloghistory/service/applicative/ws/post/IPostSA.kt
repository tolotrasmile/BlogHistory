package mg.kotleta.bloghistory.service.applicative.ws.post

import mg.kotleta.bloghistory.data.dto.PostDto


interface IPostSA {

  fun getPostById(id: Long, before: (() -> Unit)? = null, success: ((PostDto) -> Unit), failure: ((Throwable?) -> Unit), done: (() -> Unit)? = null)

  fun getPosts(before: (() -> Unit)? = null, success: ((List<PostDto>) -> Unit), failure: ((Throwable?) -> Unit), done: (() -> Unit)? = null)
}