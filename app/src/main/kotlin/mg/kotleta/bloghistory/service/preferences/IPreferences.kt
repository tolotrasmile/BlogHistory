package mg.kotleta.bloghistory.service.preferences

/**
 * Created by admin on 01/09/2017.
 */
interface IPreferences {
  fun putBool(key: String, value: Boolean)
  fun getBool(key: String): Boolean?
  fun putString(key: String, value: String)
  fun getString(key: String): String
  fun putInt(key: String, value: Int)
  fun getInt(key: String): Int
  fun getInt(key: String, defaultValue: Int): Int

}