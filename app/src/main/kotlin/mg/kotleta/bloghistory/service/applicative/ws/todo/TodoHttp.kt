package mg.kotleta.bloghistory.service.applicative.ws.todo

import mg.kotleta.bloghistory.data.constants.BeforeClosure
import mg.kotleta.bloghistory.data.constants.DoneClosure
import mg.kotleta.bloghistory.data.constants.FailureClosure
import mg.kotleta.bloghistory.data.constants.SuccessClosure
import mg.kotleta.bloghistory.data.dto.TodoDto
import mg.kotleta.bloghistory.service.applicative.ws.AbstractHttpWebService
import mg.kotleta.bloghistory.service.bdl.todo.TodoBdl


object TodoHttp : AbstractHttpWebService(), ITodoSA {
  override fun getTodos(options: Map<String, String>, before: BeforeClosure, success: SuccessClosure<List<TodoDto>>, failure: FailureClosure, done: DoneClosure) {
    performRequest(TodoBdl.getTodos(options), before, success, failure, done)
  }
}