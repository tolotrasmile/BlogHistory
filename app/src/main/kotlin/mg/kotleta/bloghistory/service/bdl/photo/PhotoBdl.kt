package mg.kotleta.bloghistory.service.bdl.photo

import io.reactivex.Observable
import mg.kotleta.bloghistory.common.RetrofitFactory
import mg.kotleta.bloghistory.data.dto.PhotoDto
import mg.kotleta.bloghistory.service.bdl.AbstractBdl

/**
 * Created by admin on 30/08/2017.
 */
object PhotoBdl : AbstractBdl(), IPhotoBdl {
  override fun getPhotos(options: Map<String, String>): Observable<List<PhotoDto>> {
    return RetrofitFactory.createService(IPhotoBdl::class.java, baseUrl).getPhotos(options)
  }
}
