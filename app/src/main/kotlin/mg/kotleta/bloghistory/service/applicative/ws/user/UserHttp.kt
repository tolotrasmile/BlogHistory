package mg.kotleta.bloghistory.service.applicative.ws.user

import mg.kotleta.bloghistory.data.dto.UserDto
import mg.kotleta.bloghistory.service.applicative.ws.AbstractHttpWebService
import mg.kotleta.bloghistory.service.bdl.user.UserBdl

object UserHttp : AbstractHttpWebService(), IUserSA {


  override fun addUser(userDto: UserDto, before: (() -> Unit)?, success: (UserDto) -> Unit, failure: (Throwable?) -> Unit, done: (() -> Unit)?) {
    performRequest(UserBdl.addUser(userDto), before, success, failure, done)
  }

  override fun getUsers(before: (() -> Unit)?, success: (List<UserDto>) -> Unit, failure: (Throwable?) -> Unit, done: (() -> Unit)?) {
    performRequest(UserBdl.getUsers(), before, success, failure, done)
  }

  override fun authenticate(params: IUserSA.AuthParametersDto, before: (() -> Unit)?, success: (UserDto) -> Unit, failure: (Throwable?) -> Unit, done: (() -> Unit)?) {

    if (params.email == "test@test.com" && params.password == "admin123") {
      success(UserDto())
    } else {
      failure(null)
    }

  }
}