package mg.kotleta.bloghistory.service.applicative.ws.photo

import mg.kotleta.bloghistory.data.constants.BeforeClosure
import mg.kotleta.bloghistory.data.constants.DoneClosure
import mg.kotleta.bloghistory.data.constants.FailureClosure
import mg.kotleta.bloghistory.data.constants.SuccessClosure
import mg.kotleta.bloghistory.data.dto.PhotoDto
import mg.kotleta.bloghistory.service.applicative.ws.AbstractHttpWebService
import mg.kotleta.bloghistory.service.bdl.photo.PhotoBdl

/**
 * Created by admin on 31/08/2017.
 */
object PhotoHttp : AbstractHttpWebService(), IPhotoSA {
  override fun getPhotos(options: Map<String, String>, before: BeforeClosure, success: SuccessClosure<List<PhotoDto>>, failure: FailureClosure, done: DoneClosure) {
    performRequest(PhotoBdl.getPhotos(options), before, success, failure, done)
  }

}