package mg.kotleta.bloghistory.service.preferences

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson

@SuppressLint("StaticFieldLeak")
/**
 * Created by admin on 25/09/2017.
 */
object KSharedPreferences {

  var context: Context? = null

  private val PREFERENCE_KEY = "mg.kotleta.bloghistory.service.preferences"

  private fun getPreferences(): SharedPreferences? {

    if (context != null) {
      return context?.getSharedPreferences(PREFERENCE_KEY, Context.MODE_PRIVATE)
    }

    return null
  }

  operator fun <T> set(key: String, value: T?) {

    val preferences = getPreferences()

    if (value == null) {
      remove(key)
    }

    preferences?.let {
      val editor = preferences.edit()
      editor.putString(key, Gson().toJson(value))
      editor.apply()
    }

  }

  operator fun <T> get(key: String, clazz: Class<T>, defaultValue: T? = null): T? {

    val preferences = getPreferences()

    preferences?.let {
      val string = preferences.getString(key, Gson().toJson(defaultValue))
      return Gson().fromJson(string, clazz)
    }

    return defaultValue
  }

  private fun remove(key: String) {

    val preferences = getPreferences()

    preferences?.let {
      val editor = preferences.edit()
      editor.remove(key)
      editor.apply()
    }

  }
}