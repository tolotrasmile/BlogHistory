package mg.kotleta.bloghistory.service.applicative.ws.todo

import mg.kotleta.bloghistory.data.constants.BeforeClosure
import mg.kotleta.bloghistory.data.constants.DoneClosure
import mg.kotleta.bloghistory.data.constants.FailureClosure
import mg.kotleta.bloghistory.data.constants.SuccessClosure
import mg.kotleta.bloghistory.data.dto.TodoDto

/**
 * Created by admin on 31/08/2017.
 */
interface ITodoSA {
  fun getTodos(options: Map<String, String> = HashMap(), before: BeforeClosure = null, success: SuccessClosure<List<TodoDto>>, failure: FailureClosure, done: DoneClosure = null)
}