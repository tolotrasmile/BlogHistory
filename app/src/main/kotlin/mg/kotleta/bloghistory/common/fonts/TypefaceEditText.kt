package mg.kotleta.bloghistory.common.fonts

import android.content.Context
import android.util.AttributeSet
import android.widget.EditText


/**
 * Created by admin on 26/09/2017.
 */
class TypefaceEditText : EditText {

  constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
    if (!isInEditMode) {
      setTypefaceFont(context, attrs)
    }
  }

  constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
    if (!isInEditMode) {
      setTypefaceFont(context, attrs)
    }
  }

  private fun setTypefaceFont(context: Context, attrs: AttributeSet) {
    FontCache.setTypeface(this, context, attrs)
  }
}
