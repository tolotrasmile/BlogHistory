package mg.kotleta.bloghistory.common.fonts

import android.content.Context
import android.util.AttributeSet

/**
 * Created by admin on 25/09/2017.
 */
class TypefaceTextView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyle: Int = 0) : android.support.v7.widget.AppCompatTextView(context, attrs, defStyle) {

  init {
    FontCache.setTypeface(this, context, attrs)
  }
}
