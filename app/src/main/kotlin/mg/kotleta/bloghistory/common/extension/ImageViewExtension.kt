package mg.kotleta.bloghistory.common.extension

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.GlideDrawable
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import mg.kotleta.bloghistory.data.constants.DoneClosure
import mg.kotleta.bloghistory.data.constants.FailureClosure
import mg.kotleta.bloghistory.data.constants.SuccessClosure
import java.lang.Exception

fun ImageView.loadImage(url: String?, listener: RequestListener<String, GlideDrawable>? = null) {
  Glide.with(context)
      .load(url)
      .listener(listener)
      .into(this)
}

fun ImageView.loadImage(url: String?) {
  Glide.with(context).load(url).into(this)
}

fun ImageView.loadImage(url: String?, done: DoneClosure = null) {
  loadImage(url, object : RequestListener<String, GlideDrawable> {
    override fun onException(e: Exception?, model: String?, target: Target<GlideDrawable>?, isFirstResource: Boolean): Boolean {
      done?.let { done() }
      return false
    }

    override fun onResourceReady(resource: GlideDrawable?, model: String?, target: Target<GlideDrawable>?, isFromMemoryCache: Boolean, isFirstResource: Boolean): Boolean {
      done?.let { done() }
      return false
    }
  })
}

fun ImageView.loadImage(url: String?, success: SuccessClosure<GlideDrawable?>? = null, failure: FailureClosure? = null) {
  loadImage(url, object : RequestListener<String, GlideDrawable> {
    override fun onException(e: Exception?, model: String?, target: Target<GlideDrawable>?, isFirstResource: Boolean): Boolean {
      failure?.let { failure(e) }
      return false
    }

    override fun onResourceReady(resource: GlideDrawable?, model: String?, target: Target<GlideDrawable>?, isFromMemoryCache: Boolean, isFirstResource: Boolean): Boolean {
      success?.let { success(resource) }
      return false
    }
  })
}