package mg.kotleta.bloghistory.common.fonts

/**
 * Created by admin on 25/09/2017.
 */

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.widget.TextView
import mg.kotleta.bloghistory.R
import java.util.*

/**
 * Created by norman on 3/8/15.
 *
 *
 * Code taken from britzl on StackOverflow (slightly modified):
 * http://stackoverflow.com/questions/16901930/memory-leaks-with-custom-font-for-set-custom-font/16902532#16902532
 */
object FontCache {

  /*
  * Caches typefaces based on their file path and name, so that they don't have to be created
  * every time when they are referenced.
  */
  private val fontCache = HashMap<String, Typeface>()

  private fun getTypeface(fontName: String, context: Context): Typeface? {

    var typeface: Typeface? = fontCache[fontName]

    if (typeface == null) {

      try {

        typeface = Typeface.createFromAsset(context.assets, fontName)

      } catch (e: Exception) {
        return null
      }

      fontCache.put(fontName, typeface)
    }

    return typeface
  }

  fun setTypeface(textView: TextView, context: Context?, attrs: AttributeSet?) {

    val array = context?.obtainStyledAttributes(attrs, R.styleable.TypefaceTextView)

    if (array != null) {

      var fontName = array.getString(R.styleable.TypefaceTextView_textViewFont)

      if (fontName == null) {
        fontName = "Roboto-Regular.ttf"
      }

      val typefaceAssetPath = "fonts/" + fontName

      val typeface = FontCache.getTypeface(typefaceAssetPath, context)

      if (typeface != null) {
        textView.setTypeface(typeface)
      }

      array.recycle()
    }
  }
}