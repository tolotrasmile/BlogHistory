package mg.kotleta.bloghistory.common

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by admin on 29/08/2017.
 */
object RetrofitFactory {
  fun <T> createService(clazz: Class<T>, endPoint: String): T {
    val builder = Retrofit.Builder()
        .baseUrl(endPoint)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
    return builder.create(clazz)
  }
}