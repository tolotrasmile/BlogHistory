package mg.kotleta.bloghistory.common.listeners

/**
 * Created by admin on 25/09/2017.
 */


interface OnItemClickListener<in T> {
  fun onItemClick(item: T?)
}