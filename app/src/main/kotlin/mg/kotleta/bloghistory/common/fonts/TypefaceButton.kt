package mg.kotleta.bloghistory.common.fonts

import android.content.Context
import android.util.AttributeSet
import android.widget.Button


/**
 * Created by admin on 26/09/2017.
 */
class TypefaceButton : Button {

  constructor(context: Context) : super(context)

  constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
    setTypefaceFont(context, attrs)
  }

  constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
    setTypefaceFont(context, attrs)
  }

  private fun setTypefaceFont(context: Context, attrs: AttributeSet) {
    FontCache.setTypeface(this, context, attrs)
  }
}
