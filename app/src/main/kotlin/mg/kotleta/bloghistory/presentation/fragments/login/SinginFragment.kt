package mg.kotleta.bloghistory.presentation.fragments.login

import android.content.Intent
import android.support.v4.content.IntentCompat
import android.widget.EditText
import android.widget.Toast
import mg.kotleta.bloghistory.R
import mg.kotleta.bloghistory.presentation.activities.MainActivity_
import mg.kotleta.bloghistory.presentation.common.AbstractActivity
import mg.kotleta.bloghistory.presentation.common.AbstractFragment
import mg.kotleta.bloghistory.service.applicative.ws.user.IUserSA
import mg.kotleta.bloghistory.service.applicative.ws.user.UserHttp
import mg.kotleta.bloghistory.service.preferences.Preferences
import org.androidannotations.annotations.Bean
import org.androidannotations.annotations.Click
import org.androidannotations.annotations.EFragment
import org.androidannotations.annotations.ViewById


@EFragment(R.layout.fragment_signin)
open class SinginFragment : AbstractFragment() {

  @ViewById(R.id.textFieldEmail)
  lateinit var textFieldEmail: EditText

  @ViewById(R.id.textFieldPassword)
  lateinit var textFieldPassword: EditText

  @Bean
  lateinit var preferences: Preferences

  @Click(R.id.buttonSignIn)
  fun signIn() {

    val auth = IUserSA.AuthParametersDto(textFieldEmail.text.toString(), textFieldPassword.text.toString())

    UserHttp.authenticate(auth, null,
        { userDto ->
          println(userDto)
          preferences.putBool("login", true)
          MainActivity_.intent(activity)
              .flags(Intent.FLAG_ACTIVITY_NEW_TASK or IntentCompat.FLAG_ACTIVITY_CLEAR_TASK)
              .start()
          activity.finish()
        },
        {
          Toast.makeText(activity.applicationContext, "Error", Toast.LENGTH_LONG).show()
        }, null
    )
  }

  @Click(R.id.forgotPassword)
  fun forgotPassword() {
    getCurrentActivity<AbstractActivity>().replaceFragment(R.id.login_container, ResetPasswordFragment.getInstance(), true, true)
  }

  companion object {
    fun getInstance(): SinginFragment_ = SinginFragment_()
  }

}
