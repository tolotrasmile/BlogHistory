package mg.kotleta.bloghistory.presentation.fragments.user

import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.RelativeLayout
import com.google.gson.Gson
import mg.kotleta.bloghistory.R
import mg.kotleta.bloghistory.common.listeners.OnItemClickListener
import mg.kotleta.bloghistory.data.dto.UserDto
import mg.kotleta.bloghistory.service.applicative.ws.user.UserHttp
import org.androidannotations.annotations.AfterViews
import org.androidannotations.annotations.Click
import org.androidannotations.annotations.EFragment
import org.androidannotations.annotations.ViewById


@EFragment(R.layout.fragment_user)
open class UsersListFragment : Fragment(), OnItemClickListener<UserDto> {

  @ViewById(R.id.recyclerView)
  lateinit var userList: RecyclerView

  @ViewById(R.id.userGrid)
  lateinit var userGrid: RecyclerView

  @Click(R.id.listReload)
  fun reload() {
    UserHttp.getUsers(before = beforeRequest, success = successUsers, failure = failureUser, done = afterRequest)
  }

  @ViewById(R.id.mainLoader)
  lateinit var mainLoader: RelativeLayout

  lateinit var userAdapter: ListUserAdapter
  lateinit var userGridAdapter: UserGridAdapter

  var items: List<UserDto>? = listOf()
    set(value) {
      value?.let {
        field = value
        userAdapter.let { userAdapter.users = value }
        userGridAdapter.let { userGridAdapter.items = value }
      }
    }

  companion object {
    fun getInstance(): UsersListFragment_ = UsersListFragment_()
  }

  @AfterViews
  fun initialize() {

    val llm = LinearLayoutManager(context)
    llm.orientation = LinearLayoutManager.VERTICAL

    userAdapter = ListUserAdapter(context, this)
    userList.adapter = userAdapter
    userList.layoutManager = llm
    userList.setHasFixedSize(true)

    val vlm = LinearLayoutManager(context)
    vlm.orientation = LinearLayoutManager.HORIZONTAL

    userGridAdapter = UserGridAdapter(context, this)
    userGrid.adapter = userGridAdapter
    userGrid.layoutManager = vlm
    userGrid.setHasFixedSize(true)

    UserHttp.getUsers(before = beforeRequest, success = successUsers, failure = failureUser, done = afterRequest)
  }

  protected val beforeRequest: () -> Unit = {
    mainLoader.visibility = View.VISIBLE
  }

  /**
   * Hide loader
   */
  protected val afterRequest: () -> Unit = {
    mainLoader.visibility = View.GONE
  }

  val successUsers: (List<UserDto>) -> Unit = { list ->
    items = list
  }

  val failureUser: (Throwable?) -> Unit = { t -> println(t.toString()) }

  override fun onItemClick(item: UserDto?) {

    item?.let {
      val builder = AlertDialog.Builder(activity)
      builder.setTitle("Confirmation")
          .setMessage("Are you sure to delete ${item.name}?")
          .setPositiveButton("Ok", { dialog, id ->
            items = items?.filter { it.id != item.id }
            print(Gson().toJson(items))
          })
          .setNegativeButton("Cancel", null)
      builder.create().show()
    }

    //Toast.makeText(context, item.toString(), Toast.LENGTH_SHORT).show()
  }

}