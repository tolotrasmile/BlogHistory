package mg.kotleta.bloghistory.presentation.common

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import mg.kotleta.bloghistory.R

abstract class AbstractActivity : FragmentActivity() {

  /**
   * Replace a given view content by a fragment
   * @param id : the view id
   * @param fragment : the fragment to display
   * @param animated : animate the view replacement
   * @param addToBackStack :  allow user to return to the old content
   */
  fun replaceFragment(id: Int, fragment: Fragment, animated: Boolean, addToBackStack: Boolean) {
    val t = supportFragmentManager.beginTransaction()

    if (animated) {
      t.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
    }

    if (addToBackStack) {
      t.addToBackStack(null)
    }

    t.replace(id, fragment)
    t.commit()
  }

  /**
   * Add a fragment in top of a given view
   * @param id : the view id
   * @param fragment : the fragment to display
   * @param animated : animate the view replacement
   * @param addToBackStack :  allow user to return to the old content
   */
  fun addFragment(id: Int, fragment: Fragment, animated: Boolean, addToBackStack: Boolean) {
    val t = supportFragmentManager.beginTransaction()

    if (animated) {
      t.setCustomAnimations(R.anim.slide_in_bottom, R.anim.stay, R.anim.stay, R.anim.slide_out_bottom)
    }

    if (addToBackStack) {
      t.addToBackStack(null)
    }

    t.replace(id, fragment)
    t.commit()
  }
}