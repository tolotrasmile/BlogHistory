package mg.kotleta.bloghistory.presentation.activities

import android.widget.ImageView
import mg.kotleta.bloghistory.R
import mg.kotleta.bloghistory.presentation.common.AbstractActivity
import mg.kotleta.bloghistory.presentation.fragments.login.NewAccountFragment
import mg.kotleta.bloghistory.presentation.fragments.login.SinginFragment
import org.androidannotations.annotations.*

@EActivity(R.layout.activity_login)
open class LoginActivity : AbstractActivity() {

  enum class LoginPage {
    SIGN_IN,
    SIGN_UP
  }

  @ViewById(R.id.buttonBack)
  lateinit var buttonBack: ImageView

  @Extra("page")
  @JvmField
  var page: LoginPage = LoginPage.SIGN_IN

  @Click(R.id.buttonBack)
  fun back() {
    onBackPressed()
  }

  @AfterViews
  fun initialize() {
    when (page) {
      LoginPage.SIGN_IN -> addFragment(R.id.login_container, SinginFragment.getInstance(), false, false)
      LoginPage.SIGN_UP -> addFragment(R.id.login_container, NewAccountFragment.getInstance(), false, false)
    }
  }

}
