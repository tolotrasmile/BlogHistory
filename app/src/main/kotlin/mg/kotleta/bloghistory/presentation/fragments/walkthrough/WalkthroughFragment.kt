package mg.kotleta.bloghistory.presentation.fragments.walkthrough

import android.view.View
import android.widget.TextView
import com.wang.avi.AVLoadingIndicatorView
import de.hdodenhof.circleimageview.CircleImageView
import mg.kotleta.bloghistory.R
import mg.kotleta.bloghistory.common.extension.loadImage
import mg.kotleta.bloghistory.presentation.common.AbstractFragment
import org.androidannotations.annotations.AfterViews
import org.androidannotations.annotations.EFragment
import org.androidannotations.annotations.ViewById

@EFragment(R.layout.fragment_walkthrough)
open class WalkthroughFragment : AbstractFragment() {

  data class WalkthroughData(val text: String = "", val imageUrl: String = "")

  var data: WalkthroughData? = null
  @ViewById(R.id.walkthroughImageViewIndicator)
  lateinit var walkthroughImageViewIndicator: AVLoadingIndicatorView

  @ViewById(R.id.walkthroughText)
  lateinit var walkthroughText: TextView

  @ViewById(R.id.walkthroughImageView)
  lateinit var walkthroughImageView: CircleImageView


  @AfterViews
  fun initialize() {

    data?.let {
      walkthroughImageView.loadImage(data?.imageUrl, { hideLoader() }, { hideLoader() })
      walkthroughText.text = data?.text
    }

  }

  fun hideLoader() {
    walkthroughImageViewIndicator.visibility = View.GONE
  }

  companion object {
    fun getInstance(data: WalkthroughData?): WalkthroughFragment_ {
      val frag = WalkthroughFragment_()
      frag.data = data
      return frag
    }
  }
}