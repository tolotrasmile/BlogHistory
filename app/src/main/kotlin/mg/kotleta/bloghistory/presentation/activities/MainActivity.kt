package mg.kotleta.bloghistory.presentation.activities

import mg.kotleta.bloghistory.R
import mg.kotleta.bloghistory.presentation.common.AbstractActivity
import mg.kotleta.bloghistory.presentation.fragments.user.UsersListFragment
import org.androidannotations.annotations.AfterViews
import org.androidannotations.annotations.EActivity

@EActivity(R.layout.activity_main)
open class MainActivity : AbstractActivity() {

  @AfterViews
  fun initialize() {
    addFragment(R.id.main_container, UsersListFragment.getInstance(), false, false)
  }

}
