package mg.kotleta.bloghistory.presentation.fragments.user

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import de.hdodenhof.circleimageview.CircleImageView
import mg.kotleta.bloghistory.R
import mg.kotleta.bloghistory.common.listeners.OnItemClickListener
import mg.kotleta.bloghistory.data.dto.UserDto

/**
 * Created by admin on 26/09/2017.
 */
class UserGridViewHolder(itemView: View?, listener: OnItemClickListener<UserDto>?) : RecyclerView.ViewHolder(itemView) {

  lateinit var userDto: UserDto

  lateinit var avatar: CircleImageView
  lateinit var name: TextView

  init {
    itemView?.let {
      itemView.setOnClickListener(View.OnClickListener { listener?.onItemClick(userDto) })
      avatar = itemView.findViewById(R.id.avatar) as CircleImageView
      name = itemView.findViewById(R.id.name) as TextView
    }
  }
}