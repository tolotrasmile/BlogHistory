package mg.kotleta.bloghistory.presentation.common

import android.support.v4.app.Fragment

abstract class AbstractFragment : Fragment() {
  fun <T> getCurrentActivity(): AbstractActivity where T : AbstractActivity = activity as AbstractActivity
}
