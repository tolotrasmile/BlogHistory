package mg.kotleta.bloghistory.presentation.activities.walkthrough

import android.support.v4.view.ViewPager
import me.relex.circleindicator.CircleIndicator
import mg.kotleta.bloghistory.R
import mg.kotleta.bloghistory.presentation.activities.LoginActivity
import mg.kotleta.bloghistory.presentation.activities.LoginActivity_
import mg.kotleta.bloghistory.presentation.common.AbstractActivity
import org.androidannotations.annotations.AfterViews
import org.androidannotations.annotations.Click
import org.androidannotations.annotations.EActivity
import org.androidannotations.annotations.ViewById


@EActivity(R.layout.activity_walkthrough)
open class WalkthroughActivity : AbstractActivity() {

  @ViewById(R.id.viewPager)
  lateinit var viewPager: ViewPager

  @ViewById(R.id.pageIndicator)
  lateinit var pageIndicator: CircleIndicator

  @Click(R.id.buttonWalkthroughSignUp)
  fun signUp() {
    goToLoginActivity(LoginActivity.LoginPage.SIGN_UP)
  }

  @Click(R.id.buttonWalkthroughSignIn)
  fun signIn() {
    goToLoginActivity(LoginActivity.LoginPage.SIGN_IN)
  }

  fun goToLoginActivity(page: LoginActivity.LoginPage) {
    LoginActivity_.intent(this).extra("page", page).start()
  }

  @AfterViews
  fun initialize() {
    viewPager.adapter = WalkthroughAdapter(supportFragmentManager)
    pageIndicator.setViewPager(viewPager)
  }
}