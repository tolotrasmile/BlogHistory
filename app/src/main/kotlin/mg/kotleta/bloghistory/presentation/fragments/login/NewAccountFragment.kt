package mg.kotleta.bloghistory.presentation.fragments.login

import mg.kotleta.bloghistory.R
import mg.kotleta.bloghistory.presentation.common.AbstractFragment
import org.androidannotations.annotations.EFragment


@EFragment(R.layout.fragment_new_account)
open class NewAccountFragment : AbstractFragment() {

  companion object {
    fun getInstance(): NewAccountFragment_ = NewAccountFragment_()
  }

}