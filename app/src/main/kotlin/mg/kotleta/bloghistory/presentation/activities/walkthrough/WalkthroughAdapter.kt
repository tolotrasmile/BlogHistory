package mg.kotleta.bloghistory.presentation.activities.walkthrough

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import mg.kotleta.bloghistory.presentation.fragments.walkthrough.WalkthroughFragment


open class WalkthroughAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

  val data: List<WalkthroughFragment.WalkthroughData> = listOf(
      WalkthroughFragment.WalkthroughData("Plan your travel anytime, anywhere. 1", "http://lorempicsum.com/rio/300/300/4"),
      WalkthroughFragment.WalkthroughData("Plan your travel anytime, anywhere. 2", "http://lorempicsum.com/rio/300/300/5"),
      WalkthroughFragment.WalkthroughData("Plan your travel anytime, anywhere. 3", "http://lorempicsum.com/rio/300/300/6")
  )

  override fun getItem(position: Int): Fragment = WalkthroughFragment.getInstance(data[position])

  override fun getCount(): Int = data.size
}