package mg.kotleta.bloghistory.presentation.fragments.login

import mg.kotleta.bloghistory.R
import mg.kotleta.bloghistory.presentation.common.AbstractFragment
import org.androidannotations.annotations.EFragment

@EFragment(R.layout.fragment_reset_password)
open class ResetPasswordFragment : AbstractFragment() {

  companion object {
    fun getInstance(): ResetPasswordFragment_ = ResetPasswordFragment_()
  }

}