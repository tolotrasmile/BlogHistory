package mg.kotleta.bloghistory.presentation.fragments.user

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import mg.kotleta.bloghistory.R
import mg.kotleta.bloghistory.common.listeners.OnItemClickListener
import mg.kotleta.bloghistory.data.dto.UserDto
import java.util.*

/**
 * Created by lovasoa_arnaud on 05/09/2017.
 */
class ListUserAdapter(val context: Context, private val listener: OnItemClickListener<UserDto>?) : RecyclerView.Adapter<UserViewHolder>() {

  var users: List<UserDto> = Collections.emptyList()
    set(value) {
      field = value
      notifyDataSetChanged()
    }
  override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): UserViewHolder {
    val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_user, parent, false)
    return UserViewHolder(view, listener)
  }

  override fun onBindViewHolder(holder: UserViewHolder?, position: Int) {
    val userDto = users[position]

    holder?.let {
      Glide.with(context).load(userDto.imageUrl()).into(holder.profil)
      holder.name.text = userDto.name
      holder.userName.text = "@${userDto.username}".toLowerCase()
      holder.userDto = userDto
    }
  }

  override fun getItemCount(): Int = users.size
}