package mg.kotleta.bloghistory.presentation.fragments.user

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import mg.kotleta.bloghistory.R
import mg.kotleta.bloghistory.common.listeners.OnItemClickListener
import mg.kotleta.bloghistory.data.dto.UserDto
import java.util.*

/**
 * Created by admin on 26/09/2017.
 */
class UserGridAdapter(val context: Context, private val listener: OnItemClickListener<UserDto>?) : RecyclerView.Adapter<UserGridViewHolder>() {

  var items: List<UserDto> = Collections.emptyList()
    set(value) {
      field = value
      notifyDataSetChanged()
    }

  override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): UserGridViewHolder? {
    val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_user_grid, parent, false)
    return UserGridViewHolder(view, listener)
  }

  override fun onBindViewHolder(holder: UserGridViewHolder?, position: Int) {
    val userDto = items[position]

    holder?.let {
      Glide.with(context).load(userDto.imageUrl()).into(holder.avatar)
      holder.name.text = userDto.name
      holder.userDto = userDto
    }
  }

  override fun getItemCount(): Int = items.size
}