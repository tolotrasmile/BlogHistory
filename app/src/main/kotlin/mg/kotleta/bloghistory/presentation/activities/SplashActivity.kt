package mg.kotleta.bloghistory.presentation.activities

import android.os.Handler
import android.os.Looper
import mg.kotleta.bloghistory.R
import mg.kotleta.bloghistory.presentation.activities.walkthrough.WalkthroughActivity_
import mg.kotleta.bloghistory.presentation.common.AbstractActivity
import mg.kotleta.bloghistory.service.preferences.Preferences
import org.androidannotations.annotations.AfterViews
import org.androidannotations.annotations.Bean
import org.androidannotations.annotations.EActivity


@EActivity(R.layout.activity_splash)
open class SplashActivity : AbstractActivity() {

  @Bean
  lateinit var preferences: Preferences

  @AfterViews
  fun initialize() {
    Handler(Looper.getMainLooper()).postDelayed({ launch() }, 3000)
  }

  private fun launch() {
    when (preferences.getBool("login")) {
      false -> WalkthroughActivity_.intent(this).start()
      true -> MainActivity_.intent(this).start()
    }
    finish()
  }
}