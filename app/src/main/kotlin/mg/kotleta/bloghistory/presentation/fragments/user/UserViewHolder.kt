package mg.kotleta.bloghistory.presentation.fragments.user

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import de.hdodenhof.circleimageview.CircleImageView
import mg.kotleta.bloghistory.R
import mg.kotleta.bloghistory.common.listeners.OnItemClickListener
import mg.kotleta.bloghistory.data.dto.UserDto

/**
 * Created by lovasoa_arnaud on 05/09/2017.
 */
class UserViewHolder(itemView: View?, listener: OnItemClickListener<UserDto>?) : RecyclerView.ViewHolder(itemView) {

  lateinit var profil: CircleImageView
  lateinit var name: TextView
  lateinit var userName: TextView

  lateinit var userDto: UserDto

  init {
    itemView?.let {
      profil = itemView.findViewById(R.id.profil) as CircleImageView
      name = itemView.findViewById(R.id.name) as TextView
      userName = itemView.findViewById(R.id.userName) as TextView
      itemView.setOnClickListener(View.OnClickListener { listener?.onItemClick(userDto) })
    }
  }
}