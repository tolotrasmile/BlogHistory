package mg.kotleta.bloghistory.data.constants


/**
 * Type alias for Web Services Requests
 */
typealias BeforeClosure = (() -> Unit)?
typealias SuccessClosure <T> = (T) -> Unit
typealias FailureClosure = (Throwable?) -> Unit
typealias DoneClosure = (() -> Unit)?
