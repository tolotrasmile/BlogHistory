package mg.kotleta.bloghistory.data.dto

class TodoDto {
  var id: Long = 0
  var userId: Long = 0
  var title: String = ""
  var completed: Boolean = false
}