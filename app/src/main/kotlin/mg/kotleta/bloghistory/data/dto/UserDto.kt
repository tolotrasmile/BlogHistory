package mg.kotleta.bloghistory.data.dto

open class UserDto {
  var id: Long = 0
  var name: String = ""
  var username: String = ""
  var email: String = ""
  var phone: String = ""
  var website: String = ""

  fun imageUrl() = "http://lorempicsum.com/rio/255/200/" + ((id % 7) + 1)

  override fun toString(): String {
    return "UserDto(id=$id, name='$name', username='$username', email='$email', phone='$phone', website='$website')"
  }
}