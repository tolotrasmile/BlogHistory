package mg.kotleta.bloghistory.data.dto

/**
 * Created by admin on 30/08/2017.
 */
open class PhotoDto {
  var id: Long = 0
  var albumId: Long = 0
  var title: String = ""
  var url: String = ""
  var thumbnailUrl: String = ""
}