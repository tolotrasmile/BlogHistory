package mg.kotleta.bloghistory.data.dto

open class PostDto {
  var id: Long = 0
  var userId: Long = 0
  var title: String = ""
  var body: String = ""
}