package mg.kotleta.bloghistory.data.dto

open class CommentDto {
  var id: Long = 0
  var postId: Long = 0
  var name: String = ""
  var email: String = ""
  var body: String = ""
}