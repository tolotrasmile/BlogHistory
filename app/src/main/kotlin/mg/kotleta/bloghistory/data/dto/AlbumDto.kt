package mg.kotleta.bloghistory.data.dto

open class AlbumDto {
  var id: Long = 0
  var userId: Long = 0
  var title: String = ""
}